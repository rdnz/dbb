# deutsche bahn barcode

## generate random

```shell
cd encode_aztec
cabal run deutsche-bahn-barcode
javac -cp 'core-3.5.1.jar:javase-3.5.1.jar:.' EncodeAztec.java
java -cp 'core-3.5.1.jar:javase-3.5.1.jar:.' EncodeAztec
```

## add test case based on real ticket `000000.pdf`

```shell
mv 000000.pdf ticket
cd ticket
./parsepdfs.sh 000000.pdf
```

then add a corresponding test case to `test/Main.hs`.

## notes

- https://itooktheredpill.irgendwo.org/2012/onlinetickets/
