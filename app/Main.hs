module Main where

import DeutscheBahnBarcode
  ( Block (Block0080BL, Header, TextFields),
    DateAndId (DateAndId),
    Property (Property),
    TextField (TextField),
    Ticket (Ticket),
    encode,
    latin1ToUtf8,
  )

import Data.ByteString qualified as B
import Data.ByteString.Builder (toLazyByteString)
import System.Random.Stateful
  (StatefulGen, UniformRange, mkStdGen, runSTGen_)
import System.Random.Stateful qualified as R
import Std hiding (id)

latestKnownId :: Int
latestKnownId = 830000000

auftragsnummerChars :: ByteString
auftragsnummerChars = "12346789ABCDEFGHKLMNOPQRSTUVWXYZ"

signature0Minimum :: Integer
signature0Minimum = 40000000000000000000000000000000000000000000000
signature0Maximum :: Integer
signature0Maximum = 700000000000000000000000000000000000000000000000
signature1Minimum :: Integer
signature1Minimum = 90000000000000000000000000000000000000000000000
signature1Maximum :: Integer
signature1Maximum = 700000000000000000000000000000000000000000000000

main :: IO ()
main = unsafePartial $
  writeFileLBS
    "in.txt"
    (latin1ToUtf8 $ toLazyByteString $ encode $ ticketRandom)

-- to do. randomize
firstName :: ByteString
firstName = "Emma"
lastName :: ByteString
lastName = "Schmidt"

ticketRandom :: (Partial) => Ticket
ticketRandom =
  Ticket signature0 signature1 $
  [
    Header (auftragsnummer <> "-3") "040420231221" "1" "DE" "DE", -- to do. randomize creation time
    Block0080BL
      "03"
      [DateAndId "05042023" "05042023" id]
      [
        Property "009" "1-1-49",
        Property "023" (lastName <> " " <> firstName),
        Property "028" (firstName <> "#" <> lastName)
      ],
    TextFields
      [
        TextField "00" "12" "03" "39" "0" "ICE  Fahrkarte\nSSP Europa Young",
        TextField "00" "52" "03" "19" "0" (lastName <> " " <> firstName <> "\n1 Erwachsener\n"),
        TextField "03" "01" "01" "50" "4" "G\xc3\xbcltigkeit 05.04.2023-05.04.2023",
        TextField "06" "13" "01" "19" "0" "Amersfoort C.",
        TextField "06" "34" "01" "19" "0" "Berlin",
        TextField "06" "66" "01" "05" "0" "2",
        TextField "08" "01" "04" "70" "0" "VIA: <1184>Hengelo*Regional Train*Bad Bentheim(Gr)<1080>RHEI**\n(05.04.2023)NV*Osnabr 17:23 ICE108/HH-Hbf 19:38 ICE699\n",
        TextField "12" "01" "03" "50" "0" "SSP Europa Young         1 BC 25 ",
        TextField "06" "32" "01" "02" "0" "->"
      ]
  ]
  where
    (signature0, signature1, auftragsnummer, id) =
      runSTGen_
        (mkStdGen 20230406)
        (\g ->
          (,,,)
            <$> uniformRM @Integer signature0Minimum signature0Maximum g
            <*> uniformRM @Integer signature1Minimum signature1Maximum g
            <*>
              (
                fmap B.pack $
                replicateM 6 $
                fmap (fromMaybe $ error "impossible. indexes from the closed interval `(0, length auftragsnummerChars - 1)`.") $
                fmap (B.indexMaybe auftragsnummerChars) $
                R.uniformRM @Int (0, B.length auftragsnummerChars - 1) g
              )
            <*>
              uniformRM
                @Int
                ((* 10 ^ (6 :: Int)) $ truncate @Double $ (/ 10 ^ (6 :: Int)) $ fromIntegral $ latestKnownId)
                ((* 10 ^ (4 :: Int)) $ ceiling @Double $ (/ 10 ^ (4 :: Int)) $ fromIntegral $ latestKnownId + 1)
                g
        )

uniformRM :: (UniformRange a, StatefulGen g m) => a -> a -> g -> m a
uniformRM = curry R.uniformRM
