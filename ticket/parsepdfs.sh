# Usage: ./parsepdf.sh *.pdf
for file in $@; do
  echo "$file: image extraction"
  pdfimages "$file" "$file"
  echo "$file: image conversion"
  for i in "$file"*.pbm "$file"*.ppm; do
    convert "$i" "$i.png"
    rm "$i"
  done
  echo "$file: barcode extraction"
  for i in "$file"*.png; do
    if file "$i" | grep -q 1-bit; then
      zxing-cmdline-runner --pure_barcode --dump_results --brief ./$i
    fi
    rm "$i"
  done
done
