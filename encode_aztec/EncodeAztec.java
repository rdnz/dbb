import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.client.j2se.MatrixToImageWriter;

import java.nio.file.Paths;
import java.nio.file.Files;
import java.util.Map;

class EncodeAztec {
  public static void main(String[] args) throws Exception {
    String content = Files.readString(Paths.get("in.txt"));
    if (content.charAt(content.length() - 1) != '\n') {
      throw new RuntimeException("missing `\n`");
    }
    content = content.substring(0, content.length() - 1);
    int width = 332;
    BitMatrix matrix =
      new MultiFormatWriter().encode(
        content,
        BarcodeFormat.AZTEC,
        width,
        width,
        Map.of(EncodeHintType.AZTEC_LAYERS, 16)
      );
    MatrixToImageWriter.writeToPath(matrix, "PNG", Paths.get("out.png"));
  }
}
