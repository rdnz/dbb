module Main where

import DeutscheBahnBarcode
  ( Block (Block0080BL, Header, TextFields),
    DateAndId (DateAndId),
    Property (Property),
    TextField (TextField),
    Ticket (Ticket),
    encode,
    latin1ToUtf8,
  )

import Data.ByteString.Builder (toLazyByteString)
import Test.Hspec (describe, hspec, it, shouldBe)
import Std

main :: IO ()
main = unsafePartial $ hspec $
  describe "encode" $ do
    it "7DE483" $
      shouldBe
        (toStrict $ latin1ToUtf8 $ toLazyByteString $ encode $ ticket7DE483)
        =<< readFileBS "ticket/7DE483.pdf-007.pbm.txt"

ticket7DE483 :: Ticket
ticket7DE483 =
  Ticket
    41878176983725831704058420440833780169766130218
    642886543966183228180392065444723868844693171933
  $
  [
    Header "7DE483-5" "020820221235" "1" "DE" "DE",
    Block0080BL
      "03"
      [DateAndId "06082022" "07082022" 725512163]
      [
        Property "009" "2-2-49",
        Property "023" (lastName <> " " <> firstName),
        Property "028" (firstName <> "#" <> lastName)
      ],
    TextFields
      [
        TextField "00" "12" "03" "39" "0" "IC/ECFahrkarte\nSuper Sparpreis EU",
        TextField "00" "52" "03" "19" "0" (lastName <> " " <> firstName <> "\n2 Erwachsene\n1 Kind9 J"),

        TextField "03" "01" "01" "50" "4" "G\xc3\xbcltigkeit 06.08.2022-07.08.2022",
        TextField "06" "13" "01" "19" "0" "Wien",
        TextField "06" "34" "01" "19" "0" "Berlin",
        TextField "06" "66" "01" "05" "0" "2",
        TextField "08" "01" "04" "70" "0" "VIA: <1181>Breclav(Gr)<1154>Breclav*Brno*(Havlickuv Brod/C.Trebova)*\nPraha*Sch\xc3\xb6na(Gr)<1080>(06.08.2022)NV*Praha 14:25 EC172\n",
        TextField "12" "01" "03" "50" "0" "Super Sparpreis EU         2 BC 25 ",
        TextField "06" "32" "01" "02" "0" "->"
      ]
  ]
  where
    firstName :: ByteString
    firstName = "G\xc3\xbcnter"
    lastName :: ByteString
    lastName = "Rothe"
