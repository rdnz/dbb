module DeutscheBahnBarcode where

import Codec.Compression.Zlib (compress)
import Data.ASN1.BinaryEncoding (DER (DER))
import Data.ASN1.Encoding (encodeASN1)
import Data.ASN1.Prim
  (ASN1 (End, IntVal, Start), ASN1ConstructionType (Sequence))
import Data.ByteString qualified as B
import Data.ByteString.Builder
  (Builder, byteString, int64Dec, intDec, lazyByteString, toLazyByteString)
import Data.ByteString.Char8 qualified as C
import Data.ByteString.Lazy qualified as L
import Data.Text.Lazy.Encoding qualified as T
import Std hiding (id)

data Ticket =
  Ticket
    {
      signature0 :: Integer,
      signature1 :: Integer,
      blocks :: [Block]
    }

data Block =
  Header
    {
      auftragsnummer :: ByteString,
      time :: ByteString,
      flags :: ByteString,
      language0 :: ByteString,
      language1 :: ByteString
    } |
  Block0080BL ByteString [DateAndId] [Property] |
  TextFields [TextField]

data DateAndId =
  DateAndId {from :: ByteString, to :: ByteString, id :: Int}

data Property =
  Property {typ :: ByteString, value :: ByteString}

data TextField =
  TextField
    {
      line :: ByteString,
      column :: ByteString,
      height :: ByteString,
      width :: ByteString,
      formatting :: ByteString,
      text :: ByteString
    }

encode :: (Partial) => Ticket -> Builder
encode (Ticket signature0 signature1 blocks) =
  "#UT01008000007" <>
  padRight
    50
    (lazyByteString $
      encodeASN1
        DER
        [
          Start Sequence,
          IntVal signature0,
          IntVal signature1,
          End Sequence
        ]
    ) <>
  padLeft 4 (int64Dec $ L.length $ blocksEncoded) <>
  lazyByteString blocksEncoded <>
  "\n"
  where
    blocksEncoded =
      compress $ toLazyByteString $ foldMap encodeBlock blocks
    encodeBlock :: (Partial) => Block -> Builder
    encodeBlock (Header auftragsnummer time flags language0 language1) =
      begin <> padLeft 4 (int64Dec $ beginLength + 4 + endLength) <> end
      where
        (beginLength, begin) = builderLength $
          "U_HEAD01"
        (endLength, end) = builderLength $
          "0080" <>
          padRight 20 (byteString auftragsnummer) <>
          byteString (time <> flags <> language0 <> language1)
    encodeBlock (Block0080BL number dateAndIds properties) =
      begin <> padLeft 4 (int64Dec $ beginLength + 4 + endLength) <> end
      where
        (beginLength, begin) = builderLength $
          "0080BL03"
        (endLength, end) = builderLength $
          byteString number <>
          padLeft 1 (intDec $ length $ dateAndIds) <>
          foldMap encodeDateAndId dateAndIds <>
          padLeft 2 (intDec $ length $ properties) <>
          foldMap encodeProperty properties
    encodeBlock (TextFields textFields) =
      begin <> padLeft 4 (int64Dec $ beginLength + 4 + endLength) <> end
      where
        (beginLength, begin) = builderLength $
          "U_TLAY01"
        (endLength, end) = builderLength $
          "RCT2" <>
          padLeft 4 (intDec $ length $ textFields) <>
          foldMap encodeTextField textFields
    encodeDateAndId :: DateAndId -> Builder
    encodeDateAndId (DateAndId from to id) =
      byteString from <> byteString to <> padRight 10 (intDec id)
    encodeProperty :: Property -> Builder
    encodeProperty (Property typ value) =
      "S" <>
      byteString typ <>
      padLeft 4 (intDec $ B.length $ value) <>
      byteString value
    encodeTextField :: TextField -> Builder
    encodeTextField  (TextField line column height width formatting text) =
      byteString (line <> column <> height <> width <> formatting) <>
      padLeft 4 (intDec $ B.length $ text) <>
      byteString text

padRight :: (Partial) => Int -> Builder -> Builder
padRight targetLength bytesOld
  | 0 <= replicateCount =
    bytes <> byteString (B.replicate replicateCount 0)
  | otherwise = error "too short target length for `padRight`"
  where
    replicateCount = targetLength - fromIntegral bytesLength
    (bytesLength, bytes) = builderLength bytesOld

padLeft :: (Partial) => Int -> Builder -> Builder
padLeft targetLength bytesOld
  | 0 <= replicateCount =
    byteString (C.replicate replicateCount '0') <> bytes
  | otherwise = error "too short target length for `padRight`"
  where
    replicateCount = targetLength - fromIntegral bytesLength
    (bytesLength, bytes) = builderLength bytesOld

builderLength :: Builder -> (Int64, Builder)
builderLength =
  (\bytes -> (L.length bytes, lazyByteString bytes)) . toLazyByteString

latin1ToUtf8 :: L.ByteString -> L.ByteString
latin1ToUtf8 = T.encodeUtf8 . T.decodeLatin1
